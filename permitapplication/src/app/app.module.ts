import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApplicationnameComponent } from './applicationname/applicationname.component';
import { ApplyforComponent } from './applyfor/applyfor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { YourPersonalComponent } from './your-personal/your-personal.component';
import { IdenticationInformationComponent } from './identication-information/identication-information.component';
import { IdcardNumberDetailsComponent } from './idcard-number-details/idcard-number-details.component';
import { YourPersonalDetailsComponent } from './your-personal-details/your-personal-details.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { SocialSecurityNumberComponent } from './social-security-number/social-security-number.component';
import { HomeComponent } from './home/home.component';
import { ImageuploadComponent } from './identication-information/imageupload/imageupload.component';
// import { ChildApplyForComponent } from './applyfor/child-apply-for/child-apply-for.component';
import { ChildYourPersonalComponent } from './your-personal/child-your-personal/child-your-personal.component';
import { ChildIdcardNumberDetailsComponent } from './idcard-number-details/child-idcard-number-details/child-idcard-number-details.component';
import { ChildYourPersonalDetailsComponent } from './your-personal-details/child-your-personal-details/child-your-personal-details.component';
import { ChildContactDetailsComponent } from './contact-details/child-contact-details/child-contact-details.component';
import { ChildSocialSecurityNumberComponent } from './social-security-number/child-social-security-number/child-social-security-number.component';

@NgModule({
  declarations: [
    AppComponent,
    ApplicationnameComponent,
    ApplyforComponent,
    YourPersonalComponent,
    IdenticationInformationComponent,
    IdcardNumberDetailsComponent,
    YourPersonalDetailsComponent,
    ContactDetailsComponent,
    SocialSecurityNumberComponent,
    HomeComponent,
    ImageuploadComponent,

    ChildYourPersonalComponent,
    ChildIdcardNumberDetailsComponent,
    ChildYourPersonalDetailsComponent,
    ChildContactDetailsComponent,
    ChildSocialSecurityNumberComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
