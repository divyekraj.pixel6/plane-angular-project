import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ApplyforComponent } from '../applyfor/applyfor.component';
import { ContactDetailsComponent } from '../contact-details/contact-details.component';
import { IdcardNumberDetailsComponent } from '../idcard-number-details/idcard-number-details.component';
import { IdenticationInformationComponent } from '../identication-information/identication-information.component';
import { SocialSecurityNumberComponent } from '../social-security-number/social-security-number.component';
import { YourPersonalDetailsComponent } from '../your-personal-details/your-personal-details.component';
import { YourPersonalComponent } from '../your-personal/your-personal.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
  @ViewChild(ApplyforComponent) ApplyforComponentGetData: ApplyforComponent
  @ViewChild(YourPersonalComponent) YourPersonalComponentGetData: YourPersonalComponent
  @ViewChild(IdenticationInformationComponent) IdenticationInformationComponentGetData: IdenticationInformationComponent
  @ViewChild(IdcardNumberDetailsComponent) IdcardNumberDetailsComponentGetData: IdcardNumberDetailsComponent
  @ViewChild(YourPersonalDetailsComponent) YourPersonalDetailsComponentGetData: YourPersonalDetailsComponent
  @ViewChild(ContactDetailsComponent) ContactDetailsComponentGetData: ContactDetailsComponent
  @ViewChild(SocialSecurityNumberComponent) SocialSecurityNumberComponentGetData: SocialSecurityNumberComponent

  applyFormValid: boolean;
  yourPersonalFormValid: boolean
  iidenticatinFormValid: boolean
  idCardNumberDetailsFormValid: boolean
  yourPersonalDetailsFormValid: boolean
  contactDetailsFormValid: boolean
  socialSecurityFormValid: boolean
  AllDataList: any = []

  constructor() { }


  ngOnInit(): void {

  }
  ngAfterViewInit() {

  }



  applyFormDataEnable(isValid: boolean) {
    this.applyFormValid = isValid;
  }
  yourPersonalFormDataEnable(isValid: boolean) {
    this.yourPersonalFormValid = isValid;
  }
  identicatinFormDataEnable(isValid: boolean) {
    this.iidenticatinFormValid = isValid;
  }
  idCardNumberDetailsFormDataEnable(isValid: boolean) {
    this.idCardNumberDetailsFormValid = isValid;
  }
  yourPersonalDetailsFormDataEnable(isValid: boolean) {
    this.yourPersonalDetailsFormValid = isValid;
  }
  contactDetailsFormDataEnable(isValid: boolean) {
    this.contactDetailsFormValid = isValid;
  }
  socialSecurityFormDataEnable(isValid: boolean) {
    this.socialSecurityFormValid = isValid;
  }

  ShowData() {
    this.AllDataList.push({
      applyfor: this.ApplyforComponentGetData.applyForForm.value.applyfor,

      lname: this.YourPersonalComponentGetData.yourPersonalForm.value.lname,
      fname: this.YourPersonalComponentGetData.yourPersonalForm.value.fname,
      dob: this.YourPersonalComponentGetData.yourPersonalForm.value.dob,
      gender: this.YourPersonalComponentGetData.yourPersonalForm.value.gender,
      nationality: this.YourPersonalComponentGetData.yourPersonalForm.value.nationality,

      driverLicense_value: this.IdenticationInformationComponentGetData.identicatinForm.value.driverLicense_value,
      leamerPermit_value: this.IdenticationInformationComponentGetData.identicatinForm.value.leamerPermit_value,
      nonDriverIDCard_value: this.IdenticationInformationComponentGetData.identicatinForm.value.nonDriverIDCard_value,

      idNo: this.IdcardNumberDetailsComponentGetData.idCardNumberDetailsForm.value.idNo,
      iddate: this.IdcardNumberDetailsComponentGetData.idCardNumberDetailsForm.value.iddate,
      typeOfLicense: this.IdcardNumberDetailsComponentGetData.idCardNumberDetailsForm.value.typeOfLicense,
      licenseIdNo: this.IdcardNumberDetailsComponentGetData.idCardNumberDetailsForm.value.licenseIdNo,

      height: this.YourPersonalDetailsComponentGetData.yourPersonalDetailsForm.value.height,
      eyeColor: this.YourPersonalDetailsComponentGetData.yourPersonalDetailsForm.value.eyeColor,
      email: this.YourPersonalDetailsComponentGetData.yourPersonalDetailsForm.value.email,
      status: this.YourPersonalDetailsComponentGetData.yourPersonalDetailsForm.value.status,
      unitCode: this.YourPersonalDetailsComponentGetData.yourPersonalDetailsForm.value.unitCode,
      streetNo: this.YourPersonalDetailsComponentGetData.yourPersonalDetailsForm.value.streetNo,
      street: this.YourPersonalDetailsComponentGetData.yourPersonalDetailsForm.value.street,
      townCity: this.YourPersonalDetailsComponentGetData.yourPersonalDetailsForm.value.townCity,
      state: this.YourPersonalDetailsComponentGetData.yourPersonalDetailsForm.value.state,
      postCode: this.YourPersonalDetailsComponentGetData.yourPersonalDetailsForm.value.postCode,
      country: this.YourPersonalDetailsComponentGetData.yourPersonalDetailsForm.value.country,

      homeNo: this.ContactDetailsComponentGetData.contactDetailsForm.value.homeNo,
      mobileNo: this.ContactDetailsComponentGetData.contactDetailsForm.value.mobileNo,
      faxNo: this.ContactDetailsComponentGetData.contactDetailsForm.value.faxNo,
      workNo: this.ContactDetailsComponentGetData.contactDetailsForm.value.workNo,
      mailChanged: this.ContactDetailsComponentGetData.contactDetailsForm.value.mailChanged,
      otherLanguage: this.ContactDetailsComponentGetData.contactDetailsForm.value.otherLanguage,

      sno: this.SocialSecurityNumberComponentGetData.socialSecurityForm.value.sno,
      listone_value: this.SocialSecurityNumberComponentGetData.socialSecurityForm.value.listone_value,
      listtwo_value: this.SocialSecurityNumberComponentGetData.socialSecurityForm.value.listtwo_value,
      listthree_value: this.SocialSecurityNumberComponentGetData.socialSecurityForm.value.listthree_value,
      listfour_value: this.SocialSecurityNumberComponentGetData.socialSecurityForm.value.listfour_value,
      listfive_value: this.SocialSecurityNumberComponentGetData.socialSecurityForm.value.listfive_value,
      listsix_value: this.SocialSecurityNumberComponentGetData.socialSecurityForm.value.listsix_value,
      listseven_value: this.SocialSecurityNumberComponentGetData.socialSecurityForm.value.listseven_value,
      signature: this.SocialSecurityNumberComponentGetData.socialSecurityForm.value.signature,
      sdate: this.SocialSecurityNumberComponentGetData.socialSecurityForm.value.sdate,



    })
  }
}
