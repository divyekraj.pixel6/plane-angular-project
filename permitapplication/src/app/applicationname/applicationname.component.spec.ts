import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationnameComponent } from './applicationname.component';

describe('ApplicationnameComponent', () => {
  let component: ApplicationnameComponent;
  let fixture: ComponentFixture<ApplicationnameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplicationnameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationnameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
