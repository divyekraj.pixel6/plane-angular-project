import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-your-personal-details',
  templateUrl: './your-personal-details.component.html',
  styleUrls: ['./your-personal-details.component.css']
})
export class YourPersonalDetailsComponent implements OnInit {
  @Output() yourPersonalDetailsFormData: EventEmitter<boolean> = new EventEmitter<boolean>();
  yourPersonalDetailsForm: FormGroup
  statusCheck: any = []
  parentData: any = []
  constructor(private fb: FormBuilder) {
    this.statusCheck = ['Single', 'Married', 'Devorced']
  }

  ngOnInit(): void {
    this.yourPersonalDetailsForm = this.fb.group({
      height: ['', [Validators.required, Validators.maxLength(10)]],
      eyeColor: ['', [Validators.required, Validators.maxLength(20)]],
      email: ['', [Validators.required, Validators.maxLength(50), Validators.email]],
      status: ['', [Validators.required]],
      unitCode: ['', [Validators.required, Validators.maxLength(20)]],
      streetNo: ['', [Validators.required, Validators.maxLength(20)]],
      street: ['', [Validators.required, Validators.maxLength(30)]],
      townCity: ['', [Validators.required, Validators.maxLength(20)]],
      state: ['', [Validators.required, Validators.maxLength(30)]],
      postCode: ['', [Validators.required, Validators.maxLength(10)]],
      country: ['', [Validators.required, Validators.maxLength(30)]],
    })
    this.yourPersonalDetailsForm.statusChanges.subscribe(value => {
      if (value === 'VALID') {
        this.yourPersonalDetailsFormData.emit(true);
      }
    });

  }
  get height() { return this.yourPersonalDetailsForm.get('height') }
  get eyeColor() { return this.yourPersonalDetailsForm.get('eyeColor') }
  get email() { return this.yourPersonalDetailsForm.get('email') }
  get status() { return this.yourPersonalDetailsForm.get('status') }
  get unitCode() { return this.yourPersonalDetailsForm.get('unitCode') }
  get streetNo() { return this.yourPersonalDetailsForm.get('streetNo') }
  get street() { return this.yourPersonalDetailsForm.get('street') }
  get townCity() { return this.yourPersonalDetailsForm.get('townCity') }
  get state() { return this.yourPersonalDetailsForm.get('state') }
  get postCode() { return this.yourPersonalDetailsForm.get('postCode') }
  get country() { return this.yourPersonalDetailsForm.get('country') }

  getData() {
    this.parentData.push(this.yourPersonalDetailsForm.value)
  }

}
