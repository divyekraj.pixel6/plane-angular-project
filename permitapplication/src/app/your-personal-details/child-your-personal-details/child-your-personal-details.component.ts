import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-child-your-personal-details',
  templateUrl: './child-your-personal-details.component.html',
  styleUrls: ['./child-your-personal-details.component.css']
})
export class ChildYourPersonalDetailsComponent implements OnInit {
  @Input() selectedValue = []
  constructor() { }

  ngOnInit(): void {
  }

}
