import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildYourPersonalDetailsComponent } from './child-your-personal-details.component';

describe('ChildYourPersonalDetailsComponent', () => {
  let component: ChildYourPersonalDetailsComponent;
  let fixture: ComponentFixture<ChildYourPersonalDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChildYourPersonalDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildYourPersonalDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
