import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YourPersonalDetailsComponent } from './your-personal-details.component';

describe('YourPersonalDetailsComponent', () => {
  let component: YourPersonalDetailsComponent;
  let fixture: ComponentFixture<YourPersonalDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YourPersonalDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YourPersonalDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
