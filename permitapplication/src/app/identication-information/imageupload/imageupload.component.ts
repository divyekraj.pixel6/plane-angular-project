import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-imageupload',
  templateUrl: './imageupload.component.html',
  styleUrls: ['./imageupload.component.css']
})
export class ImageuploadComponent implements OnInit {
  @Output() imagval: EventEmitter<any> = new EventEmitter()
  UploadImageForm: FormGroup
  url: string
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.UploadImageForm = this.fb.group({
      image: ['', [Validators.required]]
    })
  }

  imageChange(event) {
    if (event.target.files) {
      var r = new FileReader()
      r.readAsDataURL(event.target.files[0])
      r.onload = (e: any) => {
        this.url = e.target.result
      }
    }
    this.imagval.emit(true)
  }
  getData() { }

}
