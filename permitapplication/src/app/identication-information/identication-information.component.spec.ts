import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdenticationInformationComponent } from './identication-information.component';

describe('IdenticationInformationComponent', () => {
  let component: IdenticationInformationComponent;
  let fixture: ComponentFixture<IdenticationInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdenticationInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdenticationInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
