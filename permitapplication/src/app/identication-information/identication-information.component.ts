import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-identication-information',
  templateUrl: './identication-information.component.html',
  styleUrls: ['./identication-information.component.css']
})
export class IdenticationInformationComponent implements OnInit {
  @Output() identicatinFormData: EventEmitter<boolean> = new EventEmitter<boolean>();
  identicatinForm: FormGroup
  list: any = []
  constructor(private fb: FormBuilder) {
    this.list = [
      {
        discription: 'Driver License',
        name: 'driverLicense',
      },
      {
        discription: 'Leamer Permit',
        name: 'leamerPermit',
      },
      {
        discription: 'Non-Driver ID Card',
        name: 'nonDriverIDCard',
      }
    ]
  }

  ngOnInit(): void {
    this.identicatinForm = this.fb.group({
      driverLicense_value: ['', [Validators.required]],
      leamerPermit_value: ['', [Validators.required]],
      nonDriverIDCard_value: ['', [Validators.required]],
      image: ['', [Validators.required]],
    })
    this.identicatinForm.statusChanges.subscribe(value => {
      if (value === 'VALID') {
        this.identicatinFormData.emit(true);
      }
    });

  }

  getData() { }

  getImageValue(value) {
    this.identicatinForm.patchValue(
      {
        image: value
      }
    )
  }

}
