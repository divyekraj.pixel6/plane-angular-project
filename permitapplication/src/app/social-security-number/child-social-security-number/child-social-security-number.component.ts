import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-child-social-security-number',
  templateUrl: './child-social-security-number.component.html',
  styleUrls: ['./child-social-security-number.component.css']
})
export class ChildSocialSecurityNumberComponent implements OnInit {
  @Input() selectedValue = []
  constructor() { }

  ngOnInit(): void {
  }

}
