import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildSocialSecurityNumberComponent } from './child-social-security-number.component';

describe('ChildSocialSecurityNumberComponent', () => {
  let component: ChildSocialSecurityNumberComponent;
  let fixture: ComponentFixture<ChildSocialSecurityNumberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChildSocialSecurityNumberComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildSocialSecurityNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
