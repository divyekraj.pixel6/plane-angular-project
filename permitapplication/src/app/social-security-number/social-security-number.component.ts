import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-social-security-number',
  templateUrl: './social-security-number.component.html',
  styleUrls: ['./social-security-number.component.css']
})
export class SocialSecurityNumberComponent implements OnInit {
  @Output() socialSecurityFormData: EventEmitter<boolean> = new EventEmitter<boolean>();
  socialSecurityForm: FormGroup
  list: any = []
  parentData: any = []
  constructor(private fb: FormBuilder) {
    this.list = [
      {
        "id": 1,
        "discription": 'Do you ware glasses or contact lenses?',
        "name": 'listone',
      },
      {
        "id": 2,
        "discription": 'Do you have a physical or mental condition which requires that you take medication?',
        "name": 'listtwo',

      },
      {
        "id": 3,
        "discription": 'Have you ever had a seizure, blackout,or loss of consciousness?',
        "name": 'listthree',

      },
      {
        "id": 4,
        "discription": 'Do you have a physical condition which requires you to use special equipment in order to work?',
        "name": 'listfour',

      },
      {
        "id": 5,
        "discription": 'Have you ever had a seizure, blackout,or loss of consciousness?',
        "name": 'listfive',

      },
      {
        "id": 6,
        "discription": 'have you ever convicted withen the past ten years in this state or elsewhere of any offense?',
        "name": 'listsix',

      },
      {
        "id": 7,
        "discription": 'Have you resulting from your operation of,or involving, a moter vehicle?',
        "name": 'listseven',

      }
    ]
  }

  ngOnInit(): void {
    this.socialSecurityForm = this.fb.group({
      sno: ['', [Validators.required, Validators.maxLength(10)]],
      listone_value: ['', [Validators.required]],
      listtwo_value: ['', [Validators.required]],
      listthree_value: ['', [Validators.required]],
      listfour_value: ['', [Validators.required]],
      listfive_value: ['', [Validators.required]],
      listsix_value: ['', [Validators.required]],
      listseven_value: ['', [Validators.required]],
      signature: ['', [Validators.required, Validators.maxLength(30)]],
      sdate: ['', [Validators.required]],

    })
    this.socialSecurityForm.statusChanges.subscribe(value => {
      if (value === 'VALID') {
        this.socialSecurityFormData.emit(true);
      }
    });
  }
  get sno() { return this.socialSecurityForm.get('sno') }
  get signature() { return this.socialSecurityForm.get('signature') }
  get sdate() { return this.socialSecurityForm.get('sdate') }

  getData() {
    this.parentData.push(this.socialSecurityForm.value)
  }

}
