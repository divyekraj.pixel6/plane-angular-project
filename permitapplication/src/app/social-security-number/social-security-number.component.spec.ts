import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialSecurityNumberComponent } from './social-security-number.component';

describe('SocialSecurityNumberComponent', () => {
  let component: SocialSecurityNumberComponent;
  let fixture: ComponentFixture<SocialSecurityNumberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SocialSecurityNumberComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialSecurityNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
