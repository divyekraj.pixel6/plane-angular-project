import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-applyfor',
  templateUrl: './applyfor.component.html',
  styleUrls: ['./applyfor.component.css']
})
export class ApplyforComponent implements OnInit {

  applyForForm: FormGroup
  parentData: any
  list: any = []
  constructor(private fb: FormBuilder) {
    this.list = ['permit', 'idcard', 'Renewal', 'replacement']
  }

  ngOnInit(): void {
    this.applyForForm = this.fb.group({
      applyfor: ['', [Validators.required]],

    })

  }
  get applyfor() { return this.applyForForm.get('applyfor') }

  getData() {
    this.parentData = this.applyForForm.value.applyfor
  }
}
