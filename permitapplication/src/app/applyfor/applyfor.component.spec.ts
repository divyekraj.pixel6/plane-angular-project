import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyforComponent } from './applyfor.component';

describe('ApplyforComponent', () => {
  let component: ApplyforComponent;
  let fixture: ComponentFixture<ApplyforComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplyforComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplyforComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
