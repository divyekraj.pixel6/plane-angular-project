import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationnameComponent } from './applicationname/applicationname.component';
import { ApplyforComponent } from './applyfor/applyfor.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { HomeComponent } from './home/home.component';
import { IdcardNumberDetailsComponent } from './idcard-number-details/idcard-number-details.component';
import { IdenticationInformationComponent } from './identication-information/identication-information.component';
import { SocialSecurityNumberComponent } from './social-security-number/social-security-number.component';
import { YourPersonalDetailsComponent } from './your-personal-details/your-personal-details.component';
import { YourPersonalComponent } from './your-personal/your-personal.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'applicationname',
    component: ApplicationnameComponent
  },
  {
    path: 'applyfor',
    component: ApplyforComponent
  },
  {
    path: 'yourpersonal',
    component: YourPersonalComponent
  },
  {
    path: 'idcardnumberdetails',
    component: IdcardNumberDetailsComponent
  },
  {
    path: 'yourpersonaldetails',
    component: YourPersonalDetailsComponent
  },
  {
    path: 'socialsecuritynumber',
    component: SocialSecurityNumberComponent
  },
  {
    path: 'contactdetails',
    component: ContactDetailsComponent
  },
  {
    path: 'identificationinformation',
    component: IdenticationInformationComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
