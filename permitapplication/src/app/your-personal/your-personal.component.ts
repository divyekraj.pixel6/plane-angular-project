import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-your-personal',
  templateUrl: './your-personal.component.html',
  styleUrls: ['./your-personal.component.css']
})
export class YourPersonalComponent implements OnInit {
  @Output() yourPersonalFormData: EventEmitter<boolean> = new EventEmitter<boolean>();
  yourPersonalForm: FormGroup
  parentData: any = []
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.yourPersonalForm = this.fb.group({
      lname: ['', [Validators.required, Validators.maxLength(30)]],
      fname: ['', [Validators.required, Validators.maxLength(30)]],
      dob: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      nationality: ['', [Validators.required, Validators.maxLength(30)]],
    })
    this.yourPersonalForm.statusChanges.subscribe(value => {
      if (value === 'VALID') {
        this.yourPersonalFormData.emit(true);
      }
    });
  }

  get lname() { return this.yourPersonalForm.get('lname') }
  get fname() { return this.yourPersonalForm.get('fname') }
  get dob() { return this.yourPersonalForm.get('dob') }
  get gender() { return this.yourPersonalForm.get('gender') }
  get nationality() { return this.yourPersonalForm.get('nationality') }

  getData() {
    this.parentData.push(this.yourPersonalForm.value)
  }
}
