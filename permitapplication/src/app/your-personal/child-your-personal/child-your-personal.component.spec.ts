import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildYourPersonalComponent } from './child-your-personal.component';

describe('ChildYourPersonalComponent', () => {
  let component: ChildYourPersonalComponent;
  let fixture: ComponentFixture<ChildYourPersonalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChildYourPersonalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildYourPersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
