import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-child-your-personal',
  templateUrl: './child-your-personal.component.html',
  styleUrls: ['./child-your-personal.component.css']
})
export class ChildYourPersonalComponent implements OnInit {
  @Input() selectedValue = []
  constructor() { }

  ngOnInit(): void {
  }

}
