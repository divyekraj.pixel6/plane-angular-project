import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YourPersonalComponent } from './your-personal.component';

describe('YourPersonalComponent', () => {
  let component: YourPersonalComponent;
  let fixture: ComponentFixture<YourPersonalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YourPersonalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YourPersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
