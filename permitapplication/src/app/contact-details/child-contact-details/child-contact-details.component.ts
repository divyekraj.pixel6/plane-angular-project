import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-child-contact-details',
  templateUrl: './child-contact-details.component.html',
  styleUrls: ['./child-contact-details.component.css']
})
export class ChildContactDetailsComponent implements OnInit {
  @Input() selectedValue = []
  constructor() { }

  ngOnInit(): void {
  }

}
