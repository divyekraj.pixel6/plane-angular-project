import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildContactDetailsComponent } from './child-contact-details.component';

describe('ChildContactDetailsComponent', () => {
  let component: ChildContactDetailsComponent;
  let fixture: ComponentFixture<ChildContactDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChildContactDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildContactDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
