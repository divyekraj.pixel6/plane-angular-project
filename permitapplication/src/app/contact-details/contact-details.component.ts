import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.css']
})
export class ContactDetailsComponent implements OnInit {
  @Output() contactDetailsFormData: EventEmitter<boolean> = new EventEmitter<boolean>();
  contactDetailsForm: FormGroup
  parentData: any = []
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.contactDetailsForm = this.fb.group({
      homeNo: ['', [Validators.required, Validators.pattern('[6-9]{1}[0-9]{9}')]],
      mobileNo: ['', [Validators.required, Validators.pattern('[6-9]{1}[0-9]{9}')]],
      faxNo: ['', [Validators.required, Validators.maxLength(20)]],
      workNo: ['', [Validators.required, Validators.pattern('[6-9]{1}[0-9]{9}')]],
      mailChanged: ['', [Validators.required]],
      otherLanguage: ['', [Validators.required, Validators.maxLength(50)]]

    })

    this.contactDetailsForm.statusChanges.subscribe(value => {
      if (value === 'VALID') {
        this.contactDetailsFormData.emit(true);
      }
    });
  }
  get homeNo() { return this.contactDetailsForm.get('homeNo') }
  get mobileNo() { return this.contactDetailsForm.get('mobileNo') }
  get faxNo() { return this.contactDetailsForm.get('faxNo') }
  get workNo() { return this.contactDetailsForm.get('workNo') }
  get mailChanged() { return this.contactDetailsForm.get('mailChanged') }
  get otherLanguage() { return this.contactDetailsForm.get('otherLanguage') }


  getData() {
    this.parentData.push(this.contactDetailsForm.value)

  }

}
