import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-child-idcard-number-details',
  templateUrl: './child-idcard-number-details.component.html',
  styleUrls: ['./child-idcard-number-details.component.css']
})
export class ChildIdcardNumberDetailsComponent implements OnInit {
  @Input() selectedValue = []

  constructor() { }

  ngOnInit(): void {
  }

}
