import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildIdcardNumberDetailsComponent } from './child-idcard-number-details.component';

describe('ChildIdcardNumberDetailsComponent', () => {
  let component: ChildIdcardNumberDetailsComponent;
  let fixture: ComponentFixture<ChildIdcardNumberDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChildIdcardNumberDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildIdcardNumberDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
