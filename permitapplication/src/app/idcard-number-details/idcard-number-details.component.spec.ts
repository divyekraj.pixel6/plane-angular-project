import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdcardNumberDetailsComponent } from './idcard-number-details.component';

describe('IdcardNumberDetailsComponent', () => {
  let component: IdcardNumberDetailsComponent;
  let fixture: ComponentFixture<IdcardNumberDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdcardNumberDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdcardNumberDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
