import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-idcard-number-details',
  templateUrl: './idcard-number-details.component.html',
  styleUrls: ['./idcard-number-details.component.css']
})
export class IdcardNumberDetailsComponent implements OnInit {
  @Output() idCardNumberDetailsFormData: EventEmitter<boolean> = new EventEmitter<boolean>();
  idCardNumberDetailsForm: FormGroup
  parentData: any = []
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.idCardNumberDetailsForm = this.fb.group({
      idNo: ['', [Validators.required, Validators.pattern(('[0-9]{10}'))]],
      iddate: ['', [Validators.required]],
      typeOfLicense: ['', [Validators.required, Validators.maxLength(20)]],
      licenseIdNo: ['', [Validators.required, Validators.pattern(('[0-9]{10}'))]]
    })
    this.idCardNumberDetailsForm.statusChanges.subscribe(value => {
      if (value === 'VALID') {
        this.idCardNumberDetailsFormData.emit(true);
      }
    });
  }
  get idNo() { return this.idCardNumberDetailsForm.get('idNo') }
  get iddate() { return this.idCardNumberDetailsForm.get('iddate') }
  get typeOfLicense() { return this.idCardNumberDetailsForm.get('typeOfLicense') }
  get licenseIdNo() { return this.idCardNumberDetailsForm.get('licenseIdNo') }

  getData() {
    this.parentData.push(this.idCardNumberDetailsForm.value)
  }

}
